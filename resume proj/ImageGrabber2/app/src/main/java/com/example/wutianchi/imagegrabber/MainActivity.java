package com.example.wutianchi.imagegrabber;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import java.io.File;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Button;
import android.widget.TextView;

import static android.view.View.*;

public class MainActivity extends AppCompatActivity {

    private EditText mUrlEditText;

    private TableLayout mTable;

    static final int PICK_CONTACT_REQUEST = 1;


    /**
     * URL for the image that's downloaded by default if the user
     * doesn't specify otherwise.
     */
    private Uri mDefaultUrl =
            Uri.parse("http://www.dre.vanderbilt.edu/~schmidt/robot.png");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTable = (TableLayout)findViewById(R.id.TableLayout);
        mUrlEditText = (EditText)findViewById(R.id.editText);

        int permissionCheck = this.checkCallingOrSelfPermission(
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck == PackageManager.PERMISSION_DENIED) {
            this.requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    public void downloadImage(View view) {
        try {
            hideKeyboard(this, mUrlEditText.getWindowToken());

            Intent i= makeDownloadImageIntent(getUrl());
            startActivityForResult(i, PICK_CONTACT_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Hook method called back by the Android Activity framework when
     * an Activity that's been launched exits, giving the requestCode
     * it was started with, the resultCode it returned, and any
     * additional data from it.
     */
    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode,
                                    Intent data) {
        if (requestCode == PICK_CONTACT_REQUEST) {
            if (resultCode == RESULT_OK) {
                LayoutInflater inflater = getLayoutInflater();
                TableRow tr = (TableRow) inflater.inflate(R.layout.table_row, null, false);
                TextView name = (TextView) tr.findViewById(R.id.name_text);
                TextView duration = (TextView) tr.findViewById(R.id.time_text);
                Button show = (Button) tr.findViewById(R.id.show_button);

                String imageName = data.getStringExtra(
                        DownloadActivity.IMAGE_NAME_EXTRA);

                String imageTime = data.getStringExtra(
                        DownloadActivity.TIME_EXTRA);

                name.setText(imageName);
                duration.setText(imageTime);
                final Uri sth = data.getData();
                show.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // you may need to prepend "file://" to the url
                                startActivity(showImageIntent(sth));
                            }
                        });


                mTable.addView(tr);
            }
        }

    }


    /**
     * Factory method that returns an Intent for viewing the
     * downloaded image.
     */
    private Intent showImageIntent(Uri url) {

        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse("file://" + url.toString()), "image/*");
        return intent;
    }

    /**
     * Factory method that returns an Intent for downloading an image.
     */
    private Intent makeDownloadImageIntent(Uri url) {
        // Create an intent that will download the image from the web.
        // TODO -- you fill in here, replacing "null" with the proper
        // code.

        Intent intent = new Intent(this, DownloadActivity.class);
        intent.setData(url);
        return intent;
    }

    /**
     * Get the URL to download based on user input.
     */
    protected Uri getUrl() {
        Uri url = null;

        // Get the text the user typed in the edit text (if anything).
        url = Uri.parse(mUrlEditText.getText().toString());

        // If the user didn't provide a URL then use the default.
        String uri = url.toString();
        if (uri == null || uri.equals(""))
            url = mDefaultUrl;

        return url;
    }

    /**
     * This method is used to hide a keyboard after a user has
     * finished typing the url.
     */
    public void hideKeyboard(Activity activity,
                             IBinder windowToken) {
        InputMethodManager mgr =
                (InputMethodManager) activity.getSystemService
                        (Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(windowToken,
                0);
    }
}
