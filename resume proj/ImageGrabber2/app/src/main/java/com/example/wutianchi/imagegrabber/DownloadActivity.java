package com.example.wutianchi.imagegrabber;

import android.support.v7.app.AppCompatActivity;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

public class DownloadActivity extends AppCompatActivity {

    public static final String IMAGE_NAME_EXTRA = "vandy.cs251.IMAGE_NAME";
    public static final String TIME_EXTRA="vandy.cs251.TIME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Uri url = getIntent().getData();

        Runnable r = new Runnable() {

            @Override
            public void run() {
                long before = System.currentTimeMillis();
                Uri pathToFile = DownloadUtils.downloadImage(DownloadActivity.this, url);
                long after = System.currentTimeMillis();
                double duration = (double)Math.round((double)(after - before) / 100)/10;
                String dura=Double.toString(duration)+"SEC";
                Intent back= backToMA();
                back.setData(pathToFile);
                back.putExtra(TIME_EXTRA, dura);
                String imageName=process(url);
                back.putExtra(IMAGE_NAME_EXTRA, imageName);
                setResult(Activity.RESULT_OK, back);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                });
            }
        };

        Thread backgroundThread =
                new Thread(r);
        backgroundThread.start();
    }

    private Intent backToMA (){
        return new Intent(this, DownloadUtils.class);
    }

    public String process(Uri path){
        String name=path.toString();
        return name.substring(name.lastIndexOf("/"));
    }

}
