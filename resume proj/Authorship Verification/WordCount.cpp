// File name: WordCount.cpp
// Description: word count source file  


#include <cstdlib>
#include <map>
#include <set>
#include <string>
#include <iostream>
#include <fstream>
using namespace std;



const double MINIMUM_APPEARANCE = 0.001;
const int STEM_LENGTH = 6;

typedef set<string> WordSet;
typedef map<string,int> WordCountMap;


// function prototypes
WordSet readCommonWords(const string & fname);

WordCountMap processFile(const string & fname, const WordSet & common);

double compareTexts(WordCountMap & first, WordCountMap & second, const string & fname);



/* main */
int main()
{
	string commonFile = "texts/common.txt";
	string fin1, fin2, fout, str;

	do {
		cout << "Enter name of the first input file: ";
		cin >> fin1;
		fin1 = "texts/" + fin1;

		cout << "Enter name of the second input file: ";
		cin >> fin2;
		fin2 = "texts/" + fin2;

		cout << "Enter name of the output file: ";
		cin >> fout;
		fout = "texts/" + fout;

		WordSet common = readCommonWords(commonFile);

		WordCountMap first = processFile(fin1, common);
		WordCountMap second = processFile(fin2, common);

		double dist = compareTexts(first, second, fout);

		cout << "Vector Distance: " << dist << endl;

		cout << "Would you like to run the program again (Y|N): ";
		cin >> str;
	} while (tolower(str[0]) == 'y');


    return 0;
}

/* readCommonWords
 * This method reads words from a given file and places them
 * into a WordSet, which it returns.
 *
 * pre:  a file name with 1 word per line, words all in lower case
 * post: all words int the file are placed in the WordSet
 */
WordSet readCommonWords(const string & fname)
{
    WordSet ret;
	string str,word;

	ifstream infile;
    infile.open(fname.c_str());
	if (!infile) {
		cout << "Unable to open file 1. Press enter to exit program.";
		getline(cin, str);
		cin.get();
		exit(1);
	}
	while (infile >> word){
		ret.insert(word);
	}
    return ret;
}



/* processFile
 * This function reads in all words from the given file
 * after reading a word it converts it to lower case,
 * removes non alphabetic characters and stems it to STEM_LENGTH.
 * If the resulting word is considered common it is ignored.
 * Otherwise, the count in the map that matches the word is
 * is incremented.
 *
 * pre:  the name of a text file and a set of words to be ignored.
 * post: The file has been read; a map of cleansed words is created and returned
 *       
 */
WordCountMap processFile(const string & fname, const WordSet & common)
{
    WordCountMap ret;
	string str, word;
	ifstream file(fname.c_str());
	if (!file) {
		cout << "Unable to open file 2. Press enter to exit program.";
		getline(cin, str);
		cin.get();
		exit(1);
	}
	while (file >> word){
		string newWord = "";
		for (int i = 0; i < (int)word.length(); i++){
			word[i] = tolower(word[i]);
			if (isalpha(word[i])){
				newWord = newWord + word[i];
			}
		}
		if (newWord.length() > STEM_LENGTH){
			newWord = newWord.substr(0, STEM_LENGTH);
		}
		if (common.find(newWord) ==common.end() && newWord!=""){
			ret[newWord]++;
		}
	}
	
	return ret;
}



/* compareTexts
* Compares the count maps of 2 texts.
* The result returned is the euclidean distance
* where each word with a percentile appearance greater than
* MINIMUM_APPEARANCE counts as a dimesion of the vectors.
* (they still count toward the full sum used to calculate
*  percentile appearance)
*
* This method also prints out the results to a file using the following
* format (start and end tags added for clarity):
* / -- start example output --/
* word1:	distance = x.xxxxxe-xxx	(score1 = y.yyyyyyyy, score2 = z.zzzzzzz)
* word2:	distance = x.xxxxxe-xxx	(score1 = y.yyyyyyyy, score2 = z.zzzzzzz)
* ...
* wordN:	distance = x.xxxxxe-xxx	(score1 = y.yyyyyyyy, score2 = z.zzzzzzz)
*
* Counted words in document 1 = xxxx
* Counted words in document 2 = xxxx
*
* Vector Distance: dist
* /-- end example output --/
*
* word1-wordN are all words whose comparison is include in the final sum.
*             these words are ordered alphabetically.
* distance values are the square of the difference of the percentile
*                         scores from the two maps.
* score1 & score2 values are the scores from the two maps.
* dist is the final distance that is also returned,
*    it is the square root of the sum of all the individual distances.
* To help line things up, there is a tab character after the ":" and another
*    before the "(" on the line for each word.
*
* pre:  two maps of texts to be compared and the name of an output file
* post: returns the squared euclidean distance of the two files
*       if the output file cannot be opened, the method prints
*       an error mesage and exits the program.
*/
double compareTexts(WordCountMap & first, WordCountMap & second, const string & fname)
{
	ofstream outfile;
	outfile.open(fname.c_str());
	if (outfile.fail()) {
		cout << "Error opening output data file" << endl;
		exit(1);
	}
	WordCountMap::const_iterator iter;
	int sum1= 0;
	int sum2= 0;
	for (iter = first.begin();iter != first.end(); iter++) {
		sum1 = sum1 + iter->second;
	}
	for (iter = second.begin(); iter != second.end(); iter++) {
		sum2 = sum2 + iter->second;
	}
	double vectorDistance = 0.0;
	for (iter = first.begin(); iter != first.end(); iter++) {
		string word = iter->first;
		if (second.count(word) == 1){
			double score1 = (double)iter->second / sum1;
			double score2 = ((double)(second.find(word))->second) / sum2;
			double distance = (score1 - score2)*(score1 - score2);		
			if (score1 > MINIMUM_APPEARANCE && score2 > MINIMUM_APPEARANCE){
				vectorDistance = vectorDistance + distance;
				outfile << word << ":	distance = " << distance << "	(score1 = " << score1 << ", score2 = " << score2 << ")" << endl;
			}
		}
	}
	vectorDistance = sqrt(vectorDistance);
	outfile << endl;
	outfile << "Counted words in document 1 = " << sum1 << endl;
	outfile << "Counted words in document 2 = " << sum2 << endl;
	outfile << endl;
	outfile << "Vector Distance:" << vectorDistance;
	return vectorDistance; 
}
