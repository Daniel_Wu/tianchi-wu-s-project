// File name: LifoPointAgenda.h
// Description:  the interface of lifo
// Last Changed: 4/1/2015


#ifndef LifoPointAgenda_H
#define LifoPointAgenda_H
#include "Stack_t.h"
#include "PointAgenda.h"
// Your job to fill in this file!
class LifoPointAgenda : public PointAgenda{
private:
	Stack<Point> agenda;
public:
	virtual~LifoPointAgenda() {}
	// isEmpty (pure virtual)
	// Checks if the agenda is empty
	virtual bool isEmpty() const{
		return agenda.isEmpty();
	}

	// add (pure virtual)
	// adds a Point to the agenda.
	virtual void add(const Point& item){
		agenda.push(item);
	}

	// remove (pure virtual)
	// removes the next Point from the agenda.
	virtual void remove(){
		agenda.pop();
	}

	// peek (pure virtual)
	// Returns the next Point from the agenda without removing it from the agenda.
	virtual Point peek() const{
		return agenda.top();
	}

	// size (pure virtual)
	// Returns the number of Points in the agenda.
	virtual size_t size() const{
		return agenda.size();
	}

};
#endif


