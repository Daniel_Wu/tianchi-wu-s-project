// File name: Queue_t.h
// Description: the interface of queue  
// Last Changed: 4/1/2015



#ifndef QUEUE_T_H
#define QUEUE_T_H
//#include <string>
//using namespace std;
template<typename ItemType>
class Queue
{
	struct Node{
		ItemType data;
		Node *next;
	};

	typedef Node* NodePtr;
private:
	size_t mySize;
	NodePtr myFront;
	NodePtr myBack;

public:
	// Class Constructor
	Queue();

	// Copy Constructor
	// pre:  Class object, aQueue, exists
	// post: Object is initialized to be a copy of the parameter
	Queue(const Queue<ItemType>& aQueue);

	// Class Destructor
	// Destroys a queue
	// pre:  Class object exists
	// post: Class object does not exist
	~Queue();

	// Assignment operator
	// Assigns a queue to another
	// pre: both class objects exist
	// post: this class object gets assigned a copy of the parameter class object
	const Queue& operator= (const Queue<ItemType>& rhs);

	// isEmpty
	// Checks if the queue is empty
	// pre:  A queue exists.
	// post: Returns true if it IS empty, false if NOT empty.
	bool isEmpty() const;

	// enqueue
	// enqueues an item to back of the queue.
	// pre:  Queue exists and item is passed.
	// post: adds the given item to the end of the queue.
	void enqueue(const ItemType& item);

	// dequeue
	// dequeues the front item off the queue 
	// pre:  Queue exists.
	// post: Removes item on front of the queue. If the queue
	//       was already empty, throws an std::underflow_error exception.
	void dequeue();

	// front
	// Returns the front item of the queue without dequeueing it.
	// pre:  Queue exists.
	// post: Returns item at front of queue.  If the queue is empty,
	//       throws an std::underflow_error exception.
	ItemType front() const;

	// size
	// Returns the number of items on the queue.
	size_t size() const;

	/*toString
	Returns string equivalent of the stack*/
	//string toString() const;
};

// Your job to fill in this file!
#include "Queue_t.cpp"
#endif
