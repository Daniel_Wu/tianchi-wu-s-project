#include <iostream>
#include "Stack_t.h"
#include <string>
using namespace std;

int main()
{

	Stack<double> stack1;
	if (stack1.size() != 0){
		cout << "Test for default ctor1 fails" << endl;
	}
	if (stack1.toString() != ""){
		cout << "Test for default ctor2 fails" << endl;
	}

	stack1.push(1);
	stack1.push(2);
	stack1.push(3);
	Stack<double> stack2(stack1);
	if (stack2.size() != 3 && stack2.toString() != "321"){
		cout << "Test for copy ctor fails" << endl;
	}

	Stack<double> stack3= stack2;
	if (stack3.size() != 3 && stack3.toString() != "321"){
		cout << "Test for assignment operator fails" << endl;
	}

	stack3.pop();
	if (stack3.size() != 2 && stack3.toString() != "21"){
		cout << "Test for pop fails" << endl;
	}

	if (stack3.top() != 2){
		cout << "Test for top fails" << endl;
	}
	stack3.pop();
	stack3.pop();

	if (stack3.size() != 0 && stack3.toString() != "" && stack3.isEmpty()==false){
		cout << "Test for isEmpty fails" << endl;
	}


	cout << "Done testing" << endl;
	cout << "Press enter to end pgm" << endl;
	char tt;
	tt = cin.get();


//	return 0;

}