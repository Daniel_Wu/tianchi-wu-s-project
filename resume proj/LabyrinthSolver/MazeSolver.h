// File name: MazeSolver.h
// Description:  the interface of mazesolver 
// Last Changed:  4/1/2015


#ifndef MAZESOLVER_H
#define MAZESOLVER_H
#include "Maze.h"
#include "PointAgenda.h"
// Your job to fill in this file!
class MazeSolver{
private:
	Maze & maze;
	PointAgenda & agenda;
public:
	//constructor
	MazeSolver(Maze & newMaze, PointAgenda & newAgenda);
	
	//solve the maze
	bool solve(bool trace);

	void children(Point p);

	bool isValid(Point pos);

};
#endif /* ifndef */