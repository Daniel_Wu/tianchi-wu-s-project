// File name: MazeSolver.cpp
// Description:  source file to solve maze
// Last Changed:  4/1/2015

#include "MazeSolver.h"
// Your job to fill in this file!
//constructor
MazeSolver::MazeSolver(Maze & newMaze, PointAgenda & newAgenda) :maze(newMaze), agenda(newAgenda)
{

}

//solve the maze
bool MazeSolver::solve(bool trace){
	int count = 0;
	Point start=maze.getStartLocation();
	agenda.add(start);
	while (!agenda.isEmpty()){
		Point stand = agenda.peek();
		agenda.remove();
		maze.markVisited(stand);
		cout<<stand<<"->";
		count++;
		if (maze.get(stand) == '*'){
			cout << "Solution found!" << endl;
			cout << "Number of nodes visited: " << count << endl;
			return true;
		}
		children(stand);
	}
	cout << "solution not found" << endl;
	return false;
}


bool MazeSolver::isValid(Point pos){
	if (!maze.isWall(pos) &&!maze.hasBeenVisited(pos)){
		return true;
	}
	else{
		return false;
	}
}

void MazeSolver:: children(Point p){
	Point north(p.x, p.y + 1);
	if (isValid(north)){
		agenda.add(north);
	}
	Point south(p.x, p.y - 1);
	if (isValid(south)){
		agenda.add(south);
	}
	
	
	Point west(p.x - 1, p.y);
	if (isValid(west)){
		agenda.add(west);
	}
	
	Point east(p.x + 1, p.y);
	if (isValid(east)){
		agenda.add(east);
	}
}
