// File name: Queue_t.cpp
// Description: the source file of queue  
// Last Changed: 4/1/2015


// Your job to fill in this file!
#include <cstddef>
#include <stdexcept>
#include <string>
using namespace std;

// Class Constructor
template<typename ItemType>
Queue<ItemType>::Queue() :myFront(nullptr),myBack(nullptr), mySize(0)
{
}

// Copy Constructor
// pre:  Class object, aQueue, exists
// post: Object is initialized to be a copy of the parameter
template<typename ItemType>
Queue<ItemType>::Queue(const Queue& aQueue): myFront(nullptr),myBack(nullptr), mySize(0)
{
	NodePtr tmp = aQueue.myFront;
	if (tmp == nullptr){
		mySize = 0;
		myFront = nullptr;
		myBack = nullptr;
	}
	while (tmp!=nullptr)
	{
		ItemType val = tmp->data;
		enqueue(val);
		tmp = tmp->next;
	}
}

// Class Destructor
// Destroys a queue
// pre:  Class object exists
// post: Class object does not exist
template<typename ItemType>
Queue<ItemType>::~Queue(){
	while (!isEmpty())
	{
		dequeue();
	}
}

// Assignment operator
// Assigns a queue to another
// pre: both class objects exist
// post: this class object gets assigned a copy of the parameter class object
template<typename ItemType>
const Queue<ItemType>& Queue<ItemType>:: operator= (const Queue<ItemType>& rhs){
	if (this != &rhs){
		Queue tmp(rhs);
		swap(myFront, tmp.myFront);
		swap(myBack, tmp.myBack);
		swap(mySize, tmp.mySize);
	}
	return *this;
}

// isEmpty
// Checks if the queue is empty
// pre:  A queue exists.
// post: Returns true if it IS empty, false if NOT empty.
template<typename ItemType>
bool Queue<ItemType>::isEmpty() const{
	return myFront == nullptr;
}

// enqueue
// enqueues an item to back of the queue.
// pre:  
//Queue exists and item is passed.
// post: adds the given item to the end of the queue.
template<typename ItemType>
void Queue<ItemType>::enqueue(const ItemType& item){
	NodePtr newBack = new Node;
	newBack->data = item;
	newBack->next = nullptr;
	if (!isEmpty()){
		myBack->next = newBack;
		myBack = newBack;
	}
	else{
		myFront = newBack;
		myBack = newBack;
	}
	mySize++;
}

// dequeue
// dequeues the front item off the queue 
// pre:  Queue exists.
// post: Removes item on front of the queue. If the queue
//       was already empty, throws an std::underflow_error exception.
template<typename ItemType>
void Queue<ItemType>::dequeue(){
	if (isEmpty()){
		throw std::underflow_error("queue is empty");
	}
	NodePtr old = myFront;
	if (myFront == myBack){
		myFront = nullptr;
		myBack = nullptr;
	}
	else{
		myFront = myFront->next;
		old->next = nullptr;//essential
		delete old;
	}
                      mySize--;
}

// front
// Returns the front item of the queue without dequeueing it.
// pre:  Queue exists.
// post: Returns item at front of queue.  If the queue is empty,
//       throws an std::underflow_error exception.
template<typename ItemType>
ItemType Queue<ItemType>::front() const{
	if (isEmpty()){
		throw std::underflow_error("queue is empty");
	}
	else{
		return myFront->data;
	}
}

// size
// Returns the number of items on the queue.
template<typename ItemType>
size_t Queue<ItemType>::size() const{
	return mySize;
}

//toString
//Returns string equivalent of the DNA
//template<typename ItemType>
//string Queue<ItemType>::toString() const
//{
//	if (myFront == nullptr){
//		return"";
//	}
//	string result = "";
//	for (NodePtr cur = myFront; cur != nullptr; cur = cur->next){
//		result = result + to_string((int)cur->data);
//	}
//	return result;
//}