// File name: Point.h
// Description:  Represent a point via x & y coordinates.
// Last Changed: 4/1/2015

#ifndef POINT_H
#define POINT_H

#include <iostream>
using namespace std;

class Point
{
public:
	int x;
	int y;
	
	// Class Constructor
    // post: Initializes x and y to zero
    Point();

	// Class Constructor
    // post: Initializes x and y 
	Point(int x,int y);


    // Equality operators
    bool operator== (const Point& rhs) const;
    bool operator!= (const Point& rhs) const;

};

// insertion operator for output
ostream& operator<< (ostream& os, const Point &p);

#endif
