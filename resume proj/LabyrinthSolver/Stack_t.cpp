// File name: Stack_t.cpp
// Description: the source file of the stack  
// Last Changed: 4/1/2015


// Your job to fill in this file!
#include <cstddef>
#include <stdexcept>
#include <string>
using namespace std;

// Class Constructor
// post: stack is created & initialized to be empty
template <typename ItemType>
Stack<ItemType>::Stack() : myTop(nullptr), mySize(0)
{
}


// Copy Constructor
// pre: parameter object, rhs, exists
// post: stack is created to be a copy of the parameter stack
template <typename ItemType>
Stack<ItemType>::Stack(const Stack & rhs) : myTop(nullptr), mySize(rhs.mySize)
{
	if (rhs.myTop != nullptr){
		myTop = new Node;
		myTop->data = rhs.myTop->data;
		Node *newPtr = myTop;
		for (Node *origPtr = rhs.myTop->next; origPtr != nullptr; origPtr = origPtr->next){
			newPtr->next = new Node;
			newPtr = newPtr->next;
			newPtr->data = origPtr->data;
		}
		newPtr->next = nullptr;
	}
}



// Class Deconstructor
// pre: the stack exists
// post: the stack is destroyed and any dynamic memory is returned to the system
template <typename ItemType>
Stack<ItemType>:: ~Stack()
{
	while (!isEmpty())
	{
		pop();
	}
}


// Assignment operator
// Assigns a stack to another
// pre: both class objects exist
// post: this class object gets assigned a copy of the parameter class object
template <typename ItemType>
const Stack<ItemType>& Stack<ItemType>::operator= (const Stack<ItemType>& rhs)
{
	if (this != &rhs){
		Stack tmp(rhs);
		swap(myTop, tmp.myTop);
		swap(mySize, tmp.mySize);
	}
	return *this;
}


// isEmpty
// Checks if the stack is empty
// pre:  A stack exists.
// post: Returns true if it IS empty, false if NOT empty.
template <typename ItemType>
bool Stack<ItemType>::isEmpty() const
{
	return myTop == nullptr;
}


// push
// Pushes an item on top of the stack.
// pre:  Stack exists and item is passed.
// post: the item is placed on top of the stack, and size is incremented.
template <typename ItemType>
void Stack<ItemType>::push(const ItemType& item)
{
	Node *newTop = new Node;
		newTop->data = item;
		newTop->next = myTop;
		myTop = newTop;
		mySize++;
}


// pop
// Pops the top item off the stack.
// pre:  Stack exists.
// post: Removes item on top of stack.  If the stack
//       was already empty, throws a std::underflow_error exception.
template <typename ItemType>
void Stack<ItemType>::pop()
{

	if (isEmpty()){
		throw std::underflow_error("stack is empty");
	}
	else{
		Node *old = myTop;
		myTop = myTop ->next;
		mySize--;
		delete old;
	   }
}


// top
// Returns the top item of the stack without popping it.
// pre:  Stack exists.
// post: Returns item on top of stack.  If the stack
//       was already empty, throws a std::underflow_error exception.
template <typename ItemType>
ItemType Stack<ItemType>::top() const
{
	if (isEmpty()){
		throw std::underflow_error("stack is empty");
	}
	else{
		return myTop->data;
	}
}


// size
// Returns the number of items on the stack.
// post: Returns size from the private section of class.
template <typename ItemType>
size_t Stack<ItemType>::size() const
{
	return mySize;
}

//toString
//Returns string equivalent of the DNA
//template <typename ItemType>
//string Stack<ItemType>::toString() const
//{
//	if (myTop == nullptr){
//		return"";
//            }
//	string result = "";
//	for (Node *cur = myTop; cur != nullptr; cur = cur->next){
//		result = result + to_string(cur->data);
//	}
//	return result;
// }
