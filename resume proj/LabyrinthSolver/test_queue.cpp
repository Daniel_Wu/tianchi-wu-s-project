#include <iostream>
#include "Queue_t.h"
using namespace std;

int main() {
	Queue<int> queue;
	if (queue.size() != 0){
		cout << "Test for default ctor1 fails" << endl;
	}
	if (queue.toString() != ""){
		cout << "Test for default ctor2 fails" << endl;
	}
	queue.enqueue(1);
	queue.enqueue(2);
	queue.enqueue(3);

	Queue<int>queue2(queue);
	cout << queue2.toString() << endl;
	if (queue2.size() != 3 && queue2.toString() != "123"){
		cout << "Test for copy ctor fails" << endl;
		cout << queue2.toString() << endl;
	}

	Queue<int> queue3 = queue2;
	if (queue3.size() != 3 && queue3.toString() != "123"){
		cout << "Test for assignment operator fails" << endl;
		cout << queue3.toString() << endl;
	}

	queue3.dequeue();
	if (queue3.size() != 2 && queue3.toString() != "23"){
		cout << "Test for dequeue fails" << endl;
		cout << queue3.toString() << endl;
	}

	if (queue3.front() != 2){
		cout << "Test for front fails" << endl;
	}
	queue3.dequeue();
	queue3.dequeue();

	if (queue3.size() != 0 && queue3.toString() != "" && queue3.isEmpty() == false){
		cout << "Test for isEmpty fails" << endl;
	}


	cout << "Done testing" << endl;
	cout << "Press enter to end pgm" << endl;
	char tt;
	tt = cin.get();

	return 0;
}
